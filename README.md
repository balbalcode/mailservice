# Mail service komit (API MASTER)

beautiful mail service by komit.co.id

  - Simple route
  - Simple way to send mail
  - Secure and fastway

### Developed by :
  - Ikbal Maulana
  - M. Rifqi MR
  - M. Abdul Aziz

#### Built with

* Mongoose - ODM
* Express - fast node.js network app framework
* node.js - evented I/O for the backend
* MongooDB - Database architecture
* BodyParser - parsing body request for api

Please contact developer if the system has crashed or error.

### Installation


Install the dependencies and start the server. (production only)

```sh
$ cd projects
$ npm i or npm install
$ npm start
```

