const mailModel = require('../models/mailModel');

class ConfigService {
    async get(params) {
        let option = {};
        
        if (params.module) {
            option.route = params.module;
        }
        
        const getConfig = await mailModel.find(option);
        return getConfig;
    }
    
    async getByModule(configCode) {
        let option = {};
        if (configCode) {
            option.configCode = configCode;
            const getConfig = await mailModel.find(option);
            if (!getConfig) {
                throw ({status: 404, message: 'Config not found'});
            }
        }
        else{
            throw ({status: 404, message: 'Config not found'});
        }
        return getConfig;
    }
    
    async getById(id) {
        const getConfig = await mailModel.findById(id);
        if (!getConfig) {
            throw ({status: 404, message: 'Config not found'});
        }
        return getConfig;
    }
    
    async create(params) {
        let createUser = await mailModel.create(params);
        return createUser;
    }
    
    async update(id, params) {
        const option = {
            _id: id
        };
        await mailModel.updateOne(option, params);
        return this.getById(id);
    }
    
    async delete(id) {
        const getConfig = await this.getById(id);
        const response = {
            status: 200,
            message: 'Data successfull deleted',
            data: getConfig
        };
        await mailModel.deleteOne(getConfig);
        return response;
    }
}

module.exports = new ConfigService;