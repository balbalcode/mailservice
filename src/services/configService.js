const configModel = require('../models/configModel');

class ConfigService {
  async get(params) {
    let option = {};
    
    if (params) {
      option.route = params;
    }
    
    const getConfig = await configModel.find(option);
    return getConfig;
  }
  
  async getById(id) {
    const getConfig = await configModel.findById(id);
    if (!getConfig) {
      throw ({status: 404, message: 'Config not found'});
    }
    return getConfig;
  }
  
  async create(params) {
    let createUser = await configModel.create(params);
    return createUser;
  }
  
  async update(id, params) {
    const option = {
      _id: id
    };
    await configModel.updateOne(option, params);
    return this.getById(id);
  }
  
  async delete(id) {
    const getConfig = await this.getById(id);
    const response = {
      status: 200,
      message: 'Data successfull deleted',
      data: getConfig
    };
    await configModel.deleteOne(getConfig);
    return response;
  }
}

module.exports = new ConfigService;