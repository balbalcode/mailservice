const { Router } = require('express');
const router = Router();

const mailService = require('../services/mailService.js');
const configService = require('../services/configService.js');

router.get('/mail', async (req, res) => {
  try {
    const getMail = await mailService.get(req.query);
    res.status(200).send(getMail);  
  }
  catch (err) {
    res.status(400).send({
      status: 400,
      message: err.message
    });
  }
});

router.get('/mail/:module', async (req, res) => {
  try {
    const getId = await configService.get(req.params.module);
    if(!getId[0]){
      res.status(200).send({
        status: 200,
        message: "the route isn't available in storage"
      });
    }
    else{
      const getMail = await mailService.get(getId[0].route);
      res.status(200).send(getMail);  
    }
  }
  catch (err) {
    res.status(400).send({
      status: 400,
      message: err.message
    });
  }
});

router.get('/mail/:module/:id', async (req, res) => {
  try {
    const getMail = await mailService.getById(req.params.id);
    res.status(200).send(getMail);  
  }
  catch (err) {
    const statusCode = err.status ? err.status : 400;
    res.status(statusCode).send({
      status: statusCode,
      message: err.message
    });
  }
});

router.post('/mail', async (req, res) => {
  try {
    const create = await mailService.create(req.body);
    res.status(200).send(create);  
  }
  catch (err) {
    res.status(400).send({
      status: 400,
      message: err.message
    });
  }
});

router.put('/mail/:id', async (req, res) => {
  try {
    const updateUser = await mailService.update(req.params.id, req.body);
    res.status(200).send(updateUser);  
  }
  catch (err) {
    res.status(400).send({
      status: 400,
      message: err.message
    });
  }
});

router.delete('/mail/:id', async (req, res) => {
  try {
    const deleteUser = await mailService.delete(req.params.id);
    res.status(200).send(deleteUser);
  }
  catch (err) {
    res.status(400).send({
      status: 400,
      message: err.message
    });
  }
});

module.exports = router;