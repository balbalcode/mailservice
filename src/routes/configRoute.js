const { Router } = require('express');
const router = Router();

const ConfigService = require('../services/configService.js');

router.get('/config', async (req, res) => {
  try {
    const getConfig = await ConfigService.get(req.query);
    res.status(200).send(getConfig);  
  }
  catch (err) {
    res.status(400).send({
      status: 400,
      message: err.message
    });
  }
});

router.get('/config/:id', async (req, res) => {
  try {
    const getConfig = await ConfigService.getById(req.params.id);
    res.status(200).send(getConfig);  
  }
  catch (err) {
    const statusCode = err.status ? err.status : 400;
    res.status(statusCode).send({
      status: statusCode,
      message: err.message
  });
}
});

router.post('/config', async (req, res) => {
  try {
    const create = await ConfigService.create(req.body);
    res.status(200).send(create);  
  }
  catch (err) {
    res.status(400).send({
        status: 400,
        message: err.message
    });
  }
});

router.put('/config/:id', async (req, res) => {
  try {
    const updateUser = await ConfigService.update(req.params.id, req.body);
    res.status(200).send(updateUser);  
  }
  catch (err) {
    res.status(400).send({
      status: 400,
      message: err.message
    });
  }
});

router.delete('/config/:id', async (req, res) => {
  try {
    const deleteUser = await ConfigService.delete(req.params.id);
    res.status(200).send(deleteUser);
  }
  catch (err) {
    res.status(400).send({
      status: 400,
      message: err.message
    });
  }
});

module.exports = router;