const { db, uniqueValidator, idValidator } = require('../config/db.js');
const mailSchema = new db.Schema({
  code: {
    type: String,
    unique: true,
    required: true,
    dropDups: true
  },
  to: {
    type: String,
    required: true
  },
  subject: String,
  text: String,
  date: Date,
  configCode: {
    type: String,
    required: true
  },
  status: {
    type: String,
    enum: ['Pending', 'Sent', 'Failed', 'Expired' ]
  },
},
{
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

mailSchema.plugin(uniqueValidator);

module.exports = db.model('Mail', mailSchema);