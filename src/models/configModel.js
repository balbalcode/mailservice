const { db, uniqueValidator, idValidator } = require('../config/db.js');
const configSchemas = new db.Schema({
  code: {
    type: String,
    unique: true,
    required: true,
    dropDups: true
  },
  from: {
    type: String,
    unique: true,
    required: true,
    dropDups: true
  },
  name: String,
  apikey: String,
  route: {
    type: String,
    unique: true,
    required: true,
    dropDups: true
  },
  domain: String,
  username: {
    type: String,
    unique: true,
    required: true,
    dropDups: true
  },
  password: String,
  limitied: Number,
  status: {
    type: String,
    enum: ['Active', 'Not Active'],
  },
  lastTransaction: Date,
},
{
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

configSchemas.plugin(uniqueValidator);

module.exports = db.model('Config', configSchemas);