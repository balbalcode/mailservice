module.exports = (req, res, next) => {
  let { method, query, params } = req;
  let schemaPost = {
    'code': {
      notEmpty: {
        errorMessage: 'Is Required!'
      },
    },
    'from': {
      notEmpty: {
        errorMessage: 'Is Required!'
      },
    },
    'name': {
      notEmpty: {
        errorMessage: 'Is Required!'
      },
    },
    'route': {
      notEmpty: {
        errorMessage: 'Is Required!'
      },
    },
    'apikey': {
      notEmpty: {
        errorMessage: 'Is Required!'
      },
    },
    'domain': {
      notEmpty: {
        errorMessage: 'Is Required!'
      },
    },
    'username': {
      notEmpty: {
        errorMessage: 'Is Required!'
      },
    },
    'password': {
      notEmpty: {
        errorMessage: 'Is Required!'
      },
    },
    'status': {
      notEmpty: {
        errorMessage: 'Is Required!'
      },
    },
  };
  
  let schemaPut = {
    id: {
      isLength: {
        errorMessage: 'Id should be at least 24 chars long',
        options: { min: 24, max: 24 }
      }
    }
  };
  
  switch (method) {
    case 'GET':
    req.check(schemaPut);
    break;
    case 'PUT':
    req.check(schemaPut);
    break;
    case 'POST':
    req.checkBody(schemaPost);
    break;
  }
  
  req.getValidationResult().then((result) => {
    // if errors empty next step
    if (result.isEmpty()) return next();
    return res.status(400).send({
      status: 400,
      message: 'Invalid parameter',
      errors: result.array()
    });
  });
};