const { Router } = require('express');
const validators = require('validators');

module.exports = () => {
  let routes = Router();


  routes.get(`/config/:id`, validators.configValidator);
  routes.put(`/config/:id`, validators.configValidator);
  routes.post(`/config`, validators.configValidator);

  routes.get(`/mail/:module/:id`, validators.mailValidator);
  routes.put(`/mail/:id`, validators.mailValidator);
  routes.post(`/mail/`, validators.mailValidator);

  return routes;
};